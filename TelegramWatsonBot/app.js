
// ====================================================================================================================================
// ======================================================= DATABASE REPOSITORY ========================================================
// ====================================================================================================================================

//var sleep = require('sleep'); //планировалось юзать sleep.sleep(2); это работает, но то для чего предназначалось не подходит
var Cloudant = require('cloudant');

//To Store URL of Cloudant VCAP Services as found under environment variables on from App Overview page
var cloudant_url;
var services = JSON.parse(process.env.VCAP_SERVICES || "{}");
// Check if services are bound to your project
if(process.env.VCAP_SERVICES)
{
	services = JSON.parse(process.env.VCAP_SERVICES);
	if(services.cloudantNoSQLDB) //Check if cloudantNoSQLDB service is bound to your project
	{
		cloudant_url = services.cloudantNoSQLDB[0].credentials.url;  //Get URL and other paramters
		console.log("Name = " + services.cloudantNoSQLDB[0].name);
		console.log("URL = " + services.cloudantNoSQLDB[0].credentials.url);
        console.log("username = " + services.cloudantNoSQLDB[0].credentials.username);
		console.log("password = " + services.cloudantNoSQLDB[0].credentials.password);
	}
}

//Connect using cloudant npm and URL obtained from previous step
var cloudant = Cloudant({url: cloudant_url});
//Edit this variable value to change name of database.
var dbname = 'schedule_prod_db';
var db;

//Create database
cloudant.db.create(dbname, function(err, data) {
  	if(err) //If database already exists
	    console.log("Database exists. Error : ", err); //NOTE: A Database can be created through the GUI interface as well
  	else
	    console.log("Created database.");

  	//Use the database for further operations like create view, update doc., read doc., delete doc. etc, by assigning dbname to db.
  	db = cloudant.db.use(dbname);
	// https://github.com/IBM-Bluemix/nodejs-cloudantdb-crud-example/blob/master/package.json
	// https://www.ibm.com/developerworks/cloud/library/cl-guesstheword-app/
});

// READ
var arrayOfMeets = null;
var TelegramSettingsInfo = null;
function getDocuments(docType){
	// этот метод дергается после какого-либо сохранения/обновления/удаления для актуализации данных в коллекциях.
	// а так же при первом запуске (для инициализации коллекций)
	db.find({selector:{type:docType}}, function(err, data){ //поиск происходит по полю type
		if(!err) {
			console.log("Found " + data.docs.length + " documents by: ", docType);
			for (var i = 0; i < data.docs.length; i++) {
				console.log('Doc id: %s', data.docs[i]._id);
				if (docType == "token") {
					if (data.docs[i].key == "telegram")
						TelegramSettingsInfo = data.docs[i];
				}
			}
			if (docType == "meet")
				arrayOfMeets = data.docs
		} else
      		console.log("Documents not found in database");
	});
}

function getDocument(docUniqueId, docType){
	db.get(docUniqueId, function(err, data){ //поиск происходит по полю _id
		if(!err) {
			console.log("Found document : " + JSON.stringify(data));

			var globalDoc = JSON.parse(JSON.stringify(data));
			if (docType=='user') { //в этом случае docUniqueId = userId 
				userInfo[docUniqueId].events = globalDoc.events
			}
			//meets = JSON.parse(JSON.stringify(globalDoc.data));

			/*
			var eachData = JSON.parse(JSON.stringify(data), function(key, value) {
				console.log("k:"+ key + " v:" + value);
				if (key == '_rev')
					return value;
				return value;
			});
			*/
		} else
      		console.log("Document not found in database");
	});
}

// CREATE
function createDocument(newDoc){
	console.log("Try to insert");
	//delete newDoc["_rev"] //убираю, т.к. теперь его изначально не будет, в апдейте мы его добавим по надобновсти как newDoc._rev = obj._rev;
	db.insert(newDoc, function(err, data){
		if(!err) {
			console.log("Successfully inserted doc: ", JSON.stringify(newDoc));
			getDocuments(newDoc.type); // обновим коллекцию храняющую данные по данному типу документов
			if (newDoc._id != null) //а если изначально был задан идентификатор то обновим что-то по конкретному документу
				getDocument(newDoc._id, newDoc.type) 
  		} else
      		console.log("Error on Insert: ", err);
	});

}

// UPDATE
function updateDocumentByUniqueId(docUniqueId, newDoc) {
	/*var doc = {
	  BotApiTokenDoc: JSON.stringify({
		BotApiTokenDocs: [
			[{ _id: '1', key: 'token', value: '' }]
		]
	  })
	};*/
	/*var doc = {
	  BotApiTokenDoc: [{ key: 'Telegram', value: TelegramBotApiToken }]
	};*/

	if (docUniqueId != null && docUniqueId != '')
		newDoc._id = docUniqueId //приписываем _id если он есть, иначе отправим на создание документ без этого поля и он сгенерится сам

	console.log("Try to update");
	db.get(docUniqueId, function(err, body) {
		if (!err) {
			console.log("Found document : ", JSON.stringify(body));
			var obj = JSON.parse(JSON.stringify(body));
			newDoc._rev = obj._rev;
			db.insert(newDoc, function(err, body) {
				if (!err) {
					console.log("Successfully updated doc: ", JSON.stringify(newDoc));
					getDocuments(newDoc.type); // обновим коллекцию храняющую данные по данному типу документов
				} else {
					console.log("Error on Update: ", err);
				}
			});
		} else {
			console.log("Document not found in database");
			createDocument(newDoc);
		}
	});



/*	
WHY???
if use logic:
	console.log("Try to update");
	db.get(docUniqueId, function(err, body) {
		if (!err) {
			console.log("Found document : ", JSON.stringify(body));
			var obj = JSON.parse(JSON.stringify(body));
			newDoc._rev = obj._rev;
			db.insert(newDoc, function(err, body) {
				if (!err) {
					console.log("Successfully updated doc: ", JSON.stringify(newDoc));
				} else {
					console.log("Error on Update: ", err);
				}
			});
		} else {
			console.log("Document not found in database");
		}
	});

	createBotApiToken(newDoc); // иначе создадим
THEN:
When was added first row:

OUT Try to update
OUT Try to insert
OUT Document not found in database
OUT Successfully inserted doc

When she was updated:
OUT Try to update
OUT Try to insert
OUT Found document
OUT Error on Insert:  { Error: Document update conflict.
OUT Successfully updated doc
*/
}

// UPDATE
function addDocumentToDocument(docUniqueId, newDoc) {

	console.log("Try to update");
	db.get(docUniqueId, function(err, body) {
		if (!err) {
			console.log("Found global document : ", JSON.stringify(body));
			var globalDoc = JSON.parse(JSON.stringify(body));
			var newDocDataArray = JSON.parse(JSON.stringify(newDoc.data));
			globalDoc.data.push(newDocDataArray[0]); //добавляем новый элемент в массив к globalDoc.data
			//и внимание, обновляем глобальный документ
			db.insert(globalDoc, function(err, body) {
				if (!err) {
					console.log("Successfully updated doc: ", JSON.stringify(globalDoc));

					getDocuments(globalDoc.type); // обновим коллекцию храняющую данные по данному типу документов
					if (globalDoc._id != null) //а если изначально был задан идентификатор то обновим что-то по конкретному документу
						getDocument(globalDoc._id, globalDoc.type) 

				} else {
					console.log("Error on Update: ", err);
				}
			});
		} else {
			console.log("Document not found in database");
			createDocument(newDoc);
		}
	});
}

// UPDATE
function addEventToUser(docUniqueId, newDoc) {

	console.log("Try to update");
	db.get(docUniqueId, function(err, body) {
		if (!err) {
			console.log("Found global document : ", JSON.stringify(body));
			var globalDoc = JSON.parse(JSON.stringify(body));
			globalDoc.events.push(JSON.parse(JSON.stringify(newDoc))); //добавляем новый элемент в массив к globalDoc.data
			//и внимание, обновляем глобальный документ
			db.insert(globalDoc, function(err, body) {
				if (!err) {
					console.log("Successfully updated doc: ", JSON.stringify(globalDoc));

					getDocuments(globalDoc.type); // обновим коллекцию храняющую данные по данному типу документов
					if (globalDoc._id != null) //а если изначально был задан идентификатор то обновим что-то по конкретному документу
						getDocument(globalDoc._id, globalDoc.type) 
				} else {
					console.log("Error on Update: ", err);
				}
			});
		} else {
			console.log("Document not found in database");
			createDocument(newDoc);
		}
	});
}
// DELETE
function destroyDocument(docUniqueId,docType) {
	console.log("Try to delete");
	db.get(docUniqueId, function(err, body) {
		if (!err) {
			var latestRev = body._rev;
			db.destroy(docUniqueId, latestRev, function(err, body, header) {
				if (!err) {
					console.log("Successfully deleted doc: ", docUniqueId);
					getDocuments(docType); // обновим коллекцию храняющую данные по данному типу документов
				}
			});
		} else {
			console.log("Error on Delete: ", err);
		}
	});
}


// ====================================================================================================================================
// =========================================================== TELEGRAM BOT ===========================================================
// ====================================================================================================================================

var userInfo = {};
function userInfoInit(userId) {
	if (typeof userInfo[userId] == 'undefined') {
		userInfo[userId] = {}
		userInfo[userId].userId = userId
		userInfo[userId].currentState = '/start';
		userInfo[userId].events = []

		var newDoc = {_id:userId.toString(),type:'user',events:[]}
		createDocument(newDoc);
	}
}

var request = require('request');

var watson = require('./modules/watsonSettings');

var TelegramBot = require('node-telegram-bot-api')

var telegramBot = new TelegramBot(defineTelegramBotApiToken(), { polling: true });
function defineTelegramBotApiToken() {
	var defaultToken = "504376903:AAGjunKhjNGhWksPnebMJAKeQrZX6CNMdls"
	if (TelegramSettingsInfo == null)
		TelegramSettingsInfo = {_id:null,type:'token',key:'telegram',value:defaultToken}
	console.log("Default Telegram Token: ", TelegramSettingsInfo);
	return TelegramSettingsInfo.value;
};

telegramBot.on('message', function (msg) {
	console.log("Check message type");

	// здесь тоже бы лучше прослеживать чтобы userInfo[msg.chat.id] был
	userInfoInit(msg.chat.id);

	if(msg['voice']){ return onVoiceMessage(msg); }
	else if (userInfo[msg.chat.id].currentState == 'WAIT_TASK_NAME') {
		// допустим сохранили событие с именем, переводим состояние в ожидание получения времени начала
		userInfo[msg.chat.id].newEvent = {}
		userInfo[msg.chat.id].newEvent.text = msg.text;

		telegramBot.sendMessage(msg.chat.id, 'Введите время начала')
		userInfo[msg.chat.id].currentState = 'WAIT_TASK_START_DATE'
	} else if (userInfo[msg.chat.id].currentState == 'WAIT_TASK_START_DATE') {
		// допустим проапдейтили событие (добавили время начала), переводим состояние в ожидание получения времени завершения
		userInfo[msg.chat.id].newEvent.startDate = msg.text;

		telegramBot.sendMessage(msg.chat.id, 'Введите время завершения')
		userInfo[msg.chat.id].currentState = 'WAIT_TASK_END_DATE'
	} else if (userInfo[msg.chat.id].currentState == 'WAIT_TASK_END_DATE') {
		// допустим проапдейтили событие (добавили время конца)
		userInfo[msg.chat.id].newEvent.endDate = msg.text;
		
		console.log("Ne event info: ",userInfo[msg.chat.id].newEvent)
		addEventToUser(msg.chat.id, userInfo[msg.chat.id].newEvent)

		telegramBot.sendMessage(msg.chat.id, 'Событие сохранено в расписание')
		userInfo[msg.chat.id].currentState = 'END'
	}
});

function onVoiceMessage(msg){
  var chatId = msg.chat.id;	
  telegramBot.getFileLink(msg.voice.file_id).then(function(link){	
  	//setup new recognizer stream
  	var recognizeStream = watson.speech_to_text.createRecognizeStream(watson.params);
	recognizeStream.setEncoding('utf8');
  	recognizeStream.on('results', function(data){
		if(data && data.results && data.results.length>0 && data.results[0].alternatives && data.results[0].alternatives.length>0){
			var result = data.results[0].alternatives[0].transcript;
			console.log("result: ", result);
			//send speech recognizer result back to chat
			telegramBot.sendMessage(chatId, result, {
				disable_notification: true,
				reply_to_message_id: msg.message_id
			}).then(function () {
			    // reply sent!
			});
		}

	});
	['data', 'error', 'connection-close'].forEach(function(eventName){
	    recognizeStream.on(eventName, console.log.bind(console, eventName + ' event: '));
	});
	//pipe voice message to recognizer -> send to watson
  	request(link).pipe(recognizeStream);
  });
}


var mainMenu = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
	  [{ text: 'Расписание', callback_data: '1' }],
      [{ text: 'Добавить задачу', callback_data: '2' }],
      [{ text: 'Список мероприятий', callback_data: '3' }] //Добавить мероприятие
    ]
  })
};

telegramBot.onText(/\/start/, function (msg) {
  userInfoInit(msg.chat.id);
  console.log("New user: ",msg.chat.id)

  telegramBot.sendMessage(msg.chat.id, 'Выберите любую кнопку:', mainMenu);
});

telegramBot.on('callback_query', function onCallbackQuery(callbackQuery) { //userInfo[callbackQuery.from.id] == userInfo[msg.chat.id] 
	if (userInfo[callbackQuery.from.id].currentState == "WAIT_MEET_ID") {

		// допустим приписали в раписписание id мероприятия
		telegramBot.sendMessage(callbackQuery.from.id, 'Мероприятие сохранено в расписание')
		userInfo[callbackQuery.from.id].currentState = 'END'

	} else if (callbackQuery.data=='1') {

		console.log('Prepare list user tasks')
		var body = [];
		for (var i = 0; i < userInfo[callbackQuery.from.id].events.length; i++){
			var stringify = userInfo[callbackQuery.from.id].events[i].text + " (" + userInfo[callbackQuery.from.id].events[i].startDate + "-" + userInfo[callbackQuery.from.id].events[i].endDate + ")"
			body[i] = {text:stringify, callback_data:'E'+i}
		}
		var eventsMenu = {
		  reply_markup: JSON.stringify({
			inline_keyboard: [
			  body
			]
		  })
		};
		telegramBot.sendMessage(callbackQuery.from.id, 'Список личных задач:', eventsMenu)

	} else if (callbackQuery.data=='2') {

		telegramBot.sendMessage(callbackQuery.from.id, 'Введите наименование задачи')
		userInfo[callbackQuery.from.id].currentState = 'WAIT_TASK_NAME'

	} else if (callbackQuery.data=='3') {

		var body = [];
		for (var i = 0; i < arrayOfMeets.length; i++){
			var stringify = arrayOfMeets[i].MeetName + " (" + arrayOfMeets[i].MeetDateStart + "-" + arrayOfMeets[i].MeetDateEnd + ")"
			body[i] = {text:stringify, callback_data:arrayOfMeets[i]._id}
		}
		var meetsMenu = {
		  reply_markup: JSON.stringify({
			inline_keyboard: [
			  body
			]
		  })
		};
		telegramBot.sendMessage(callbackQuery.from.id, 'Список ближайших мероприятий:', meetsMenu) //убираю "Выберите мероприятие", т.к. убираю идею заноса в расписание - теперь просто покажем список мероприятий
		//userInfo[callbackQuery.from.id].currentState = 'WAIT_MEET_ID'

	}
});

//a chatId is a unique identifier for a chat, that can be either private, group, supergroup or channel whereas userId is a unique identifier for a user or telegramBot only
// Если сообщение отправлял пользователь, то свойство msg.chat.id, если же он кликал на кнопку, то msg.from.id

/*eslint-env node*/

// ====================================================================================================================================
// ====================================================== WEB APP STARTER BLUEMIX =====================================================
// ====================================================================================================================================

//------------------------------------------------------------------------------
// node.js starter application for Bluemix
//------------------------------------------------------------------------------

// This application uses express as its web server
// for more info, see: http://expressjs.com
var express = require('express');
var bodyParser = require("body-parser");
// создаем парсер для данных application/x-www-form-urlencoded
var urlencodedParser = bodyParser.urlencoded({extended: false}); // for submit button
var jsonParser = bodyParser.json(); // for ajax

// cfenv provides access to your Cloud Foundry environment
// for more info, see: https://www.npmjs.com/package/cfenv
var cfenv = require('cfenv');

// create a new express server
var app = express();

// serve the files out of ./public as our main files
app.use(express.static(__dirname + '/public'));

// Set view path
var path = require('path');
app.set('views', path.join(__dirname, 'public'));
// set up ejs for templating. You can use whatever
app.set('view engine', 'ejs');

// get the app environment from Cloud Foundry
var appEnv = cfenv.getAppEnv();

// start server on the specified port and binding host
app.listen(appEnv.port, '0.0.0.0', function() {

 // print a message when the server starts listening
 console.log("server starting on " + appEnv.url);
});

// ====================================================================================================================================
// ======================================================= WEB APP CONTROLLERS ========================================================
// ====================================================================================================================================

// example get param:
//app.get("/api/users/:id", function(req, res){
// when method is called from ajax need use: res.send(<some jbject>);

app.get('/', (req, res) => {
	console.log("/");
	//начитаем данные (только отдать их сразу отсюда не выйдет
	//, т.к. начитаются после завершения работы корневой функции (гребаный reduce) - потом обратимся поворно и заберем их через ajax)
	getDocuments("token");
	getDocuments("meet");
	res.render('index', {TelegramBotApiToken:TelegramSettingsInfo.value});
});

// управляем токенами:
app.post("/SaveOrUpdateTelegramBotApiToken", jsonParser, function(req, res){ //when npt ajax then urlencodedParser instead jsonParser
	console.log("/SaveOrUpdateTelegramBotApiToken");
	if(!req.body)
		return res.sendStatus(400);
	console.log("FormData: ", req.body);
	//здесь было бы идеально пересоздать бота с новым апи токеном, но почему-то нельзя
	// готовим документ
	var newDoc = {
	  type: TelegramSettingsInfo.type
	  ,key: TelegramSettingsInfo.key
	  ,value:req.body.TelegramBotApiToken
	};
	updateDocumentByUniqueId(TelegramSettingsInfo._id, newDoc); // сохраняем/обновляем
	res.json(req.body.TelegramBotApiToken); // when not ajax then res.redirect("/"); instead res.json(
});

// загружаем ajax ом наши данные
app.post('/loadMeets', jsonParser, function(req, res) {
	console.log("/loadMeets");
	if(!req.body)
		return res.sendStatus(400);
	console.log("FormData: ", req.body);

	//определим индекс
	var index = (req.body.arrayOfMeetsIndex >= arrayOfMeets.length) ? 0 : req.body.arrayOfMeetsIndex;
	var jsonResp = {};
	if (arrayOfMeets.length != 0) {
		jsonResp = arrayOfMeets[index];
		jsonResp.index = index;
	}
	console.log("Response: ", jsonResp);
	res.json(jsonResp);
});

// новое мероприятие
app.post("/SaveOrUpdateMeet", jsonParser, function(req, res){ //when npt ajax then urlencodedParser instead jsonParser
	console.log("/SaveOrUpdateMeet");
	if(!req.body)
		return res.sendStatus(400);
	console.log("FormData: ", req.body);

	// готовим документ
	var newDoc = {
	  type: "meet"
	  ,MeetName:req.body.MeetName
	  ,MeetDateStart:req.body.MeetDateStart
	  ,MeetDateEnd:req.body.MeetDateEnd
	};
	updateDocumentByUniqueId(req.body.MeetId,newDoc); // сохраняем/обновляем

	res.json(req.body.MeetName); // when not ajax then res.redirect("/"); instead res.json(
});

app.post("/DeleteMeet", jsonParser, function(req, res){ //when npt ajax then urlencodedParser instead jsonParser
	console.log("/DeleteMeet");
	if(!req.body)
		return res.sendStatus(400);
	console.log("FormData: ", req.body);

	if (req.body.MeetId != null && req.body.MeetId != '')
		destroyDocument(req.body.MeetId,"meet");
	
	res.json(req.body.MeetId); // when not ajax then res.redirect("/"); instead res.json(
});


