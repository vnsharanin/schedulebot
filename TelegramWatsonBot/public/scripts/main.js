
		// ============================================================================================
		// ============================================ TOKENS ========================================
		// ============================================================================================

		function onSaveOrUpdateTelegramBotApiToken() {
			var TelegramBotApiToken = $("#TelegramBotApiTokenId").val();
			$.ajax({
                type: "POST",
                url: "/SaveOrUpdateTelegramBotApiToken",
                data: JSON.stringify({TelegramBotApiToken: TelegramBotApiToken}),
                dataType: "json",
                contentType: "application/json",
                success: function(data){
					$("#SaveOrUpdateTelegramBotApiTokenResult").text("Token "+data+" was successuffly setted!");
                },
				error: function(data){ 
                    $("#SaveOrUpdateTelegramBotApiTokenResult").text("Error! Token was not setted!");
                }
            });
		}

		// ============================================================================================
		// ============================================= MEET =========================================
		// ============================================================================================

		var arrayOfMeetsIndex = 0;

		function startPageInit(){
			showSaveMeetForm();
		}

		function onNextMeet(){
			arrayOfMeetsIndex += 1;
			loadMeets(arrayOfMeetsIndex);
		}

		function showSaveMeetForm() {
			//глобальный м.б. скрыт, показываем его
			$("#MeetsDiv").show();

			//скрываем форму апдейта и показываем форму создания
			$("#updateMeetForm").hide();
			$("#saveMeetForm").show();

			//сброс значений:
			$("#MeetId").val('');
			$("#MeetNameId").val('');
			$("#MeetDateStartId").val('');
			$("#MeetDateEndId").val('');
			arrayOfMeetsIndex = 0;
		}

		function showUpdateMeetForm(){
			$("#saveMeetForm").hide();
			$("#updateMeetForm").show();
			loadMeets(arrayOfMeetsIndex);
		}

		function onSaveMeet(){
			onSaveOrUpdateMeet();
		}

		function onUpdateMeet(){
			if ($("#MeetId").val()!='')
				onSaveOrUpdateMeet();
			else
				$("#MeetOperationResult").text("Error! Update for non existable record!");
		}

		function onSaveOrUpdateMeet() {
			var MeetId = $("#MeetId").val();
			var MeetName = $("#MeetNameId").val();
			var MeetDateStart = $("#MeetDateStartId").val();
			var MeetDateEnd = $("#MeetDateEndId").val();
			$.ajax({
                type: "POST",
                url: "/SaveOrUpdateMeet",
                data: JSON.stringify({MeetId:MeetId,MeetName:MeetName, MeetDateStart:MeetDateStart, MeetDateEnd:MeetDateEnd}),
                dataType: "json",
                contentType: "application/json",
                success: function(data){
					$("#MeetOperationResult").text("Meet was successuffly saved/updated!");
                },
				error: function(data){ 
                    $("#MeetOperationResult").text("Error! Meet was not saved/updated!");
                }
            });
		}

		function onDeleteMeet() {
			var MeetId = $("#MeetId").val();
			if (MeetId == null || MeetId == '') {
				$("#MeetOperationResult").text("Error! Delete for non existable record!");
				return;
			}
			$.ajax({
                type: "POST",
                url: "/DeleteMeet",
                data: JSON.stringify({MeetId:MeetId}),
                dataType: "json",
                contentType: "application/json",
                success: function(data){
					$("#MeetOperationResult").text("Meet was successuffly deleted!");
					loadMeets(0);
                },
				error: function(data){ 
                    $("#MeetOperationResult").text("Error! Meet was not deleted!");
                }
            });
		}

		function loadMeets(meetIndex) {
		//с GET проблемы по отправке параметра (не вижу его на сервере, а уже почти дедлайн, поэтому временно делаю через пост)
			$.ajax({
                type: "POST",
                url: "/loadMeets",
                data: JSON.stringify({arrayOfMeetsIndex:meetIndex}),
                dataType: "json",
                contentType: "application/json",
                success: function(data){
					//for (var i = 0; i < data.length; i++)
					//	var obj = data[i].MeetName;
					if (data._id != null && data._id != '') {
						$("#MeetId").val(data._id)
						$("#MeetNameId").val(data.MeetName)
						$("#MeetDateStartId").val(data.MeetDateStart)
						$("#MeetDateEndId").val(data.MeetDateEnd)
						arrayOfMeetsIndex = data.index
					} else {
						showSaveMeetForm();
						$("#MeetsDiv").hide();
						$("#MeetOperationResult").text("Meets list is empty!");
					}
                },
				error: function(data){ 
                    $("#MeetOperationResult").text("Error! Can't load meet");
                }
            });
		}